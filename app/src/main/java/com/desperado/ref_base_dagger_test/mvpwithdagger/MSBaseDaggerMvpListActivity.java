package com.desperado.ref_base_dagger_test.mvpwithdagger;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.desperado.ref_base_dagger_test.di.DaggerContract;
import com.desperado.ref_base_dagger_test.di.DaggerContractDelegate;
import com.desperado.ref_base_dagger_test.di.component.ApplicationComponent;
import com.desperado.ref_base_dagger_test.di.module.ActivityModule;

import javax.inject.Inject;

/**
 * Created by robin on 17-3-4.
 * modified by desperado on 2017/5/2
 * 1.使用Dagger注入P
 * 2.分别是实现{@link DaggerContract.com.topview.xxt.base.di.DaggerContract.AppComponent}和
 * {@link DaggerContract.com.topview.xxt.base.di.DaggerContract.AcModule}, 用于获取AppComponent和
 * ActivityHasPModule
 */

public abstract class MSBaseDaggerMvpListActivity<P extends MSBaseRxContract.BasePresenter> extends AppCompatActivity
        implements DaggerContract.AppComponent, DaggerContract.AcModule {

    private static final String TAG = MSBaseDaggerMvpListActivity.class.getSimpleName();

    @Inject
    protected P mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent();
        onCreatePresenter();
        init(mPresenter, savedInstanceState);
    }

    abstract protected void setupComponent();

    private void onCreatePresenter() {
        if (mPresenter != null) {
            mPresenter.onCreate();
        }
    }

    abstract protected void init(P presenter, Bundle savedInstanceState);

    final public P getPresenter() {
        return mPresenter;
    }

    @Override
    @CallSuper
    protected void onResume() {
        super.onResume();
        if (mPresenter != null)
            mPresenter.onResume();
    }

    @Override
    @CallSuper
    protected void onStop() {
        super.onStop();
        if (mPresenter != null)
            mPresenter.onStop();
    }

    @Override
    @CallSuper
    protected void onPause() {
        super.onPause();
        if (mPresenter != null)
            mPresenter.onPause();
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDestroy();
            mPresenter.detachView();
        }
    }

    @Override
    public ApplicationComponent getApplicationComponent() {
        return DaggerContractDelegate.getAppComponent(getApplication());
    }

    @Override
    public ActivityModule getAcModule() {
        return DaggerContractDelegate.getAcModule(this);
    }
}
