package com.desperado.ref_base_dagger_test.mvpwithdagger;

import android.content.Context;
import android.support.annotation.CallSuper;

import com.desperado.ref_base_dagger_test.lifecircle.RxPresenter;

import java.lang.ref.WeakReference;

;

/**
 * ActivityContract 基类
 * 所有mvp的View与Presenter应直接或间接继承这其中的两个接口
 * Created by robin on 17-2-11.
 */
public interface MSBaseRxContract {

    interface BaseView<P extends BasePresenter> {
    }

    /**
     * Presenter基类
     * 使用弱引用保存View类，防止Activity的内存泄漏
     * 使用defaultView，让getView()方法永远不为空，应在子类中实现getDefaultView()并返回一个空实现的View
     * 关联生命周期
     *
     * @param <V> V
     * modified by desperado on 2017/4/27
     * 继承{@link com.desperado.ref_base_dagger_test.lifecircle.RxPresenter}, 使得该类支持RxLifeCircle
     */
    abstract class BasePresenter<V extends BaseView> extends RxPresenter {
        private WeakReference<V> mView;
        private Context mContext;
        private V defaultView = null;

        public BasePresenter(Context context, V view) {
            attachView(context, view);
        }

        public V getView() {
            V view;
            if (mView != null && (view = mView.get()) != null)
                return view;

            if (defaultView == null)
                defaultView = getDefaultView();
            return defaultView;
        }

        protected V getDefaultView() {
            return null;
        }

        public Context getApplicationContext() {
            return mContext;
        }

        public final void attachView(Context context, V view) {
            mView = new WeakReference<>(view);
            mContext = context.getApplicationContext();
            onAttachView();
        }

        protected void onAttachView() {
        }

        public final void detachView() {
            onDetachView();
            if (mView != null) {
                mView.clear();
                mView = null;
            }
        }

        /**
         * 关闭一些资源或请求
         * 如activity退出后结束所有正在进行的网络请求
         */
        protected void onDetachView() {
        }

        @Override
        @CallSuper
        public void onCreate() {
            super.onCreate();
        }

        public void onResume() {
        }

        public void onPause() {
        }

        public void onStop() {
        }

        @Override
        @CallSuper
        public void onDestroy() {
            super.onDestroy();
        }
    }
}
