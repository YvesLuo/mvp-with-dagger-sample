package com.desperado.ref_base_dagger_test.justactivity.di;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.di.module.ActivityModule;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;
import com.desperado.ref_base_dagger_test.justactivity.JustActivityWithP;

import dagger.Component;

/**
 * Created by root on 17-5-2.
 */
@PerActivity
@Component(dependencies = CustomApplicationComponent.class,modules = {ActivityModule.class, JustActivityWithPModule.class})
public interface JustActivityWithPComponent {

    void inject(JustActivityWithP activityWithP);

}
