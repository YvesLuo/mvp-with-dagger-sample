package com.desperado.ref_base_dagger_test.justactivity.di;

import android.app.Activity;

import com.desperado.ref_base_dagger_test.di.scope.PerActivity;
import com.desperado.ref_base_dagger_test.justactivity.JustActivityWithPContract;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 17-5-2.
 */
@Module
public class JustActivityWithPModule {

    @Provides
    @PerActivity
    @Named("nameInActivity")
    String providesName() {
        return "小罗";
    }

    @Provides
    @PerActivity
    JustActivityWithPContract.View providesView(Activity activityWithP) {
        return (JustActivityWithPContract.View) activityWithP;
    }
}
