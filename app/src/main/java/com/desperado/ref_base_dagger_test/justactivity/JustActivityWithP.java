package com.desperado.ref_base_dagger_test.justactivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.desperado.ref_base_dagger_test.R;
import com.desperado.ref_base_dagger_test.justactivity.di.DaggerJustActivityWithPComponent;
import com.desperado.ref_base_dagger_test.justactivity.di.JustActivityWithPModule;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpActivity;

/**
 * Created by root on 17-5-2.
 */

public class JustActivityWithP extends MSBaseDaggerMvpActivity<JustPresenter> implements JustActivityWithPContract.View {

    @Override
    public void showName(String name) {
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void setupComponent() {
        DaggerJustActivityWithPComponent.builder()
                .customApplicationComponent(getCustomAppComponent())
                .activityModule(getAcModule())
                .justActivityWithPModule(new JustActivityWithPModule())
                .build()
                .inject(this);
    }

    @Override
    protected void init(JustPresenter presenter, Bundle savedInstanceState) {
        setContentView(R.layout.activity_just_with_p);
    }

    public void show(View view) {
        getPresenter().getName();
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, JustActivityWithP.class);
        context.startActivity(starter);
    }

    public void showSingleName(View view) {
        getPresenter().showSingleName();
    }
}
