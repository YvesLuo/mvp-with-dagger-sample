package com.desperado.ref_base_dagger_test.justfragment.di;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.di.module.FragmentModule;
import com.desperado.ref_base_dagger_test.di.scope.PerFragment;
import com.desperado.ref_base_dagger_test.justfragment.JustFragmentWithP;

import dagger.Component;

/**
 * Created by root on 17-5-4.
 */
@PerFragment
@Component(dependencies = CustomApplicationComponent.class, modules = {FragmentModule.class, JustFragmentModule.class})
public interface JustFragmentComponent {

    void inject(JustFragmentWithP fragmentWithP);
}
