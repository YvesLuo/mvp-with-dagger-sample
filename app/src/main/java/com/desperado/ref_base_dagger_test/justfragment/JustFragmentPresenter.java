package com.desperado.ref_base_dagger_test.justfragment;

import android.content.Context;

import com.desperado.ref_base_dagger_test.di.scope.PerFragment;

import javax.inject.Inject;


/**
 * Created by root on 17-5-4.
 */
@PerFragment
public class JustFragmentPresenter extends JustFragmentWithPContract.Presenter {

    @Inject
    public JustFragmentPresenter(Context context, JustFragmentWithPContract.View view) {
        super(context, view);
    }

    @Override
    public void showName() {
        getView().showName("小小罗");
    }
}
