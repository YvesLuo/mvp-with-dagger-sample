package com.desperado.ref_base_dagger_test.justfragment.di;

import android.support.v4.app.Fragment;

import com.desperado.ref_base_dagger_test.di.scope.PerFragment;
import com.desperado.ref_base_dagger_test.justfragment.JustFragmentWithPContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 17-5-4.
 */
@Module
public class JustFragmentModule {

    @Provides
    @PerFragment
    JustFragmentWithPContract.View providesView(Fragment fragment) {
        return (JustFragmentWithPContract.View) fragment;
    }

}
