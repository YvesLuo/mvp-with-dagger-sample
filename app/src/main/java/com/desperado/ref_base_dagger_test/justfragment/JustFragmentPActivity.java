package com.desperado.ref_base_dagger_test.justfragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.desperado.ref_base_dagger_test.R;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpActivity;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseRxContract;

/**
 * Created by root on 17-5-4.
 */

public class JustFragmentPActivity extends MSBaseDaggerMvpActivity {

    @Override
    protected void setupComponent() {
        //empty
    }

    @Override
    protected void init(MSBaseRxContract.BasePresenter presenter, Bundle savedInstanceState) {
        setContentView(R.layout.activity_just_fragment_p);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.just_fragment_ll_container, new JustFragmentWithP())
                .commit();
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, JustFragmentPActivity.class);
        context.startActivity(starter);
    }
}
