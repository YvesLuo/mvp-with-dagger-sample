package com.desperado.ref_base_dagger_test.commonp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.desperado.ref_base_dagger_test.R;
import com.desperado.ref_base_dagger_test.commonp.di.DaggerSharePComponent;
import com.desperado.ref_base_dagger_test.commonp.di.SharePComponent;
import com.desperado.ref_base_dagger_test.di.DaggerContract;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpActivity;

/**
 * Created by root on 17-5-2.
 */

public class SharePActivity extends MSBaseDaggerMvpActivity<SharePresenter> implements Contract.View,
        DaggerContract.HasComponent<SharePComponent> {

    private SharePComponent mComponent;

    @Override
    public void showName(String name) {
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void setupComponent() {
        mComponent = DaggerSharePComponent.builder()
                .customApplicationComponent(getCustomAppComponent())
                .activityModule(getAcModule())
                .build();
        mComponent.inject(this);
    }

    @Override
    protected void init(SharePresenter presenter, Bundle savedInstanceState) {
        setContentView(R.layout.activity_share_p);
    }

    public void showName(View view) {
        getPresenter().showName();
    }

    @Override
    public SharePComponent getComponent() {
        return mComponent;
    }

    public void showFragment(View view) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.share_p_container, new SharePFragment())
                .commit();
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, SharePActivity.class);
        context.startActivity(starter);
    }
}
