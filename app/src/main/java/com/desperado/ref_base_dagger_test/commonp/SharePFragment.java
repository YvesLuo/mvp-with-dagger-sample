package com.desperado.ref_base_dagger_test.commonp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.desperado.ref_base_dagger_test.R;
import com.desperado.ref_base_dagger_test.commonp.di.SharePComponent;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpFragment;

/**
 * Created by root on 17-5-2.
 */

public class SharePFragment extends MSBaseDaggerMvpFragment<SharePresenter> {

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_share_p, container, false);
        Button mBtButton = (Button) view.findViewById(R.id.share_bt_p);
        mBtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().showName();
            }
        });
        return view;
    }

    @Override
    protected void setupComponent() {
        getAttachActivityComponent(SharePComponent.class).inject(this); //获取宿主Activity的Component实现注入P,注入的是同一个实例
    }

    @Override
    protected void init(SharePresenter presenter, Bundle saveInstanceState) {

    }
}
