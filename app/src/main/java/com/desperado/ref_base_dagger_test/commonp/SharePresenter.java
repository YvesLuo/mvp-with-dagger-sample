package com.desperado.ref_base_dagger_test.commonp;

import android.content.Context;

import com.desperado.ref_base_dagger_test.di.qualifier.ActivityContext;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by root on 17-5-2.
 */
@PerActivity
public class SharePresenter extends Contract.Presenter {

    private String mName;

    @Inject
    public SharePresenter(@ActivityContext Context context, Contract.View view, @Named("inShareActivity")
            String mName) {
        super(context, view);
        this.mName = mName;
    }

    @Override
    public void showName() {
        getView().showName(mName);
    }
}
