package com.desperado.ref_base_dagger_test.commonp.di;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.commonp.SharePActivity;
import com.desperado.ref_base_dagger_test.commonp.SharePFragment;
import com.desperado.ref_base_dagger_test.di.module.ActivityModule;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by root on 17-5-2.
 */
@PerActivity
@Component(dependencies = CustomApplicationComponent.class, modules = {ActivityModule.class, SharePModule.class})
public interface SharePComponent {

    void inject(SharePActivity activity);

    void inject(SharePFragment fragment);

}
