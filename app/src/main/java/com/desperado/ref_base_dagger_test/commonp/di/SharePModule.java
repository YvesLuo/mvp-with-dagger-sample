package com.desperado.ref_base_dagger_test.commonp.di;

import android.app.Activity;

import com.desperado.ref_base_dagger_test.commonp.Contract;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;
import com.desperado.ref_base_dagger_test.di.scope.PerFragment;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 17-5-2.
 */
@Module
public class SharePModule {

    @PerActivity
    @Provides
    @Named("inShareActivity")
    String providesName() {
        return "大罗";
    }

    @PerFragment
    @Provides
    @Named("inShareFragment")
    String providesNameInFragment() {
        return "齐达内";
    }

    @PerActivity
    @Provides
    Contract.View providesView(Activity activity) {
        return (Contract.View) activity;
    }

}
