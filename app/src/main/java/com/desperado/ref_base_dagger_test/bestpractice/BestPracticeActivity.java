package com.desperado.ref_base_dagger_test.bestpractice;

import android.os.Bundle;

import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpActivity;

/**
 * Created by root on 17-5-4.
 */

public class BestPracticeActivity extends MSBaseDaggerMvpActivity<BestPracticePresenter> {

    @Override
    protected void setupComponent() {

    }

    @Override
    protected void init(BestPracticePresenter presenter, Bundle savedInstanceState) {

    }
}
