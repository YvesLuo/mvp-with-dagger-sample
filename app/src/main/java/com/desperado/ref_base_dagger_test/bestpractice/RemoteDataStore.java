package com.desperado.ref_base_dagger_test.bestpractice;

import android.util.Log;

import com.desperado.ref_base_dagger_test.app.NetworkManager;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import javax.inject.Inject;

/**
 * Created by root on 17-5-6.
 */
@PerActivity
public class RemoteDataStore {

    private NetworkManager networkManager;

    @Inject
    public RemoteDataStore(NetworkManager manager) {
        this.networkManager = manager;
    }

    public void fetchDataFromNetRemote() {
        Log.d(GlobalLogTag.TAG, "fetchDataFromNetRemote: ");
        networkManager.fetchDataFromNetwork();
    }
}
