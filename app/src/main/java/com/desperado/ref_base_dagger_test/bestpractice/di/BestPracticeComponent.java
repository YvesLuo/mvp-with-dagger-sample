package com.desperado.ref_base_dagger_test.bestpractice.di;

import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.bestpractice.BestPracticeActivity;
import com.desperado.ref_base_dagger_test.di.module.ActivityModule;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by root on 17-5-4.
 */
@PerActivity
@Component(dependencies = CustomApplicationComponent.class, modules = {ActivityModule.class, BestPracticeModule.class})
public interface BestPracticeComponent {

    void inject(BestPracticeActivity activity);

}
