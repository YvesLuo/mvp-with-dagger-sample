package com.desperado.ref_base_dagger_test.bestpractice.di;

import android.app.Activity;

import com.desperado.ref_base_dagger_test.bestpractice.BestPracticeContract;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 17-5-4.
 */
@Module
public class BestPracticeModule {

    @Provides
    @PerActivity
    BestPracticeContract.View providesView(Activity activity) {
        return (BestPracticeContract.View) activity;
    }

}
