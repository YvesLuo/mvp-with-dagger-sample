package com.desperado.ref_base_dagger_test.bestpractice;

import android.util.Log;

import com.desperado.ref_base_dagger_test.app.DbManager;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import javax.inject.Inject;

/**
 * Created by root on 17-5-6.
 */
@PerActivity
public class LocalDataStore {

    private DbManager dbManager;

    @Inject
    public LocalDataStore(DbManager dbManager) {
        this.dbManager = dbManager;
    }

    public void fetchDataFromLocal() {
        Log.d(GlobalLogTag.TAG, "fetchDataFromLocal: ");
        dbManager.fetchDataFromDBb();
    }
}
