package com.desperado.ref_base_dagger_test.bestpractice;

import android.content.Context;

import com.desperado.ref_base_dagger_test.app.SingleClassInApp;
import com.desperado.ref_base_dagger_test.di.qualifier.ActivityContext;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import javax.inject.Inject;

/**
 * Created by root on 17-5-4.
 */
@PerActivity
public class BestPracticePresenter extends BestPracticeContract.Presenter {

    private SingleClassInApp mSingleClassInApp;

    private DataRepository mDataRepository;

    @Inject
    public BestPracticePresenter(@ActivityContext Context context, BestPracticeContract.View view,
                                 SingleClassInApp singleClassInApp, DataRepository repository) {
        super(context, view);
        this.mSingleClassInApp = singleClassInApp;
        this.mDataRepository = repository;
    }

    @Override
    public void fetchData() {
        mDataRepository.fetchData();
    }

    @Override
    public void refresh() {
        mDataRepository.refreshData();
    }

}
