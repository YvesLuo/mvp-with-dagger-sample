package com.desperado.ref_base_dagger_test.bestpractice;

import android.util.Log;

import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import javax.inject.Inject;

/**
 * Created by root on 17-5-6.
 */
@PerActivity
public class DataRepository {

    private LocalDataStore localDataStore;

    private RemoteDataStore remoteDataStore;

    @Inject
    public DataRepository(LocalDataStore localDataStore, RemoteDataStore remoteDataStore) {
        this.localDataStore = localDataStore;
        this.remoteDataStore = remoteDataStore;
    }

    public void fetchData() {
        Log.d(GlobalLogTag.TAG, "fetchData: hit data from local");
        localDataStore.fetchDataFromLocal();
    }

    public void refreshData() {
        Log.d(GlobalLogTag.TAG, "fetchData: hit data from remote");
        remoteDataStore.fetchDataFromNetRemote();
    }
}
