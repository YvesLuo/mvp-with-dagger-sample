package com.desperado.ref_base_dagger_test.bothhasp;

import android.content.Context;

import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseRxContract;

/**
 * Created by root on 17-5-4.
 */

public interface FragmentContract {

    interface View extends MSBaseRxContract.BaseView<Presenter> {
        void showName(String name);
    }

    abstract class Presenter extends MSBaseRxContract.BasePresenter<View> {

        public Presenter(Context context, View view) {
            super(context, view);
        }

        abstract void showName();
    }
}
