package com.desperado.ref_base_dagger_test.bothhasp;

import android.content.Context;

import com.desperado.ref_base_dagger_test.di.scope.PerFragment;

import javax.inject.Inject;

/**
 * Created by root on 17-5-4.
 */
@PerFragment
public class FragmentPresenter extends FragmentContract.Presenter {

    @Inject
    public FragmentPresenter(Context context, FragmentContract.View view) {
        super(context, view);
    }

    @Override
    void showName() {
        getView().showName("272");
    }
}
