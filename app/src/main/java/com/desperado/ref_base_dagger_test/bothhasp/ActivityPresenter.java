package com.desperado.ref_base_dagger_test.bothhasp;

import android.content.Context;

import com.desperado.ref_base_dagger_test.di.qualifier.ActivityContext;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import javax.inject.Inject;

/**
 * Created by root on 17-5-4.
 */
@PerActivity
public class ActivityPresenter extends ActivityContract.Presenter {

    @Inject
    public ActivityPresenter(@ActivityContext Context context, ActivityContract.View view) {
        super(context, view);
    }

    @Override
    void showName() {
        getView().showName("马塞洛");
    }
}
