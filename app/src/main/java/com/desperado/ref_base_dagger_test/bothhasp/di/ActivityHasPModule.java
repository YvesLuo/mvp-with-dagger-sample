package com.desperado.ref_base_dagger_test.bothhasp.di;

import android.app.Activity;

import com.desperado.ref_base_dagger_test.bothhasp.ActivityContract;
import com.desperado.ref_base_dagger_test.di.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 17-5-4.
 */
@Module
public class ActivityHasPModule {

    @Provides
    @PerActivity
    ActivityContract.View providesView(Activity activity) {
        return (ActivityContract.View) activity;
    }
}
