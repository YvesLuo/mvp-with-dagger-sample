package com.desperado.ref_base_dagger_test.bothhasp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.desperado.ref_base_dagger_test.R;
import com.desperado.ref_base_dagger_test.bothhasp.di.DaggerFragmentComponent;
import com.desperado.ref_base_dagger_test.mvpwithdagger.MSBaseDaggerMvpFragment;

/**
 * Created by root on 17-5-4.
 */

public class BothHasPFragment extends MSBaseDaggerMvpFragment<FragmentPresenter> implements FragmentContract.View {

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_both_has_p, container, false);
        view.findViewById(R.id.both_p_bt_showName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().showName();
            }
        });
        return view;
    }

    @Override
    protected void setupComponent() {
        DaggerFragmentComponent.builder()
                .customApplicationComponent(getCustomAppComponent())
                .fragmentModule(getFragmentModule())
                .build()
                .inject(this);
    }

    @Override
    protected void init(FragmentPresenter presenter, Bundle saveInstanceState) {

    }

    @Override
    public void showName(String name) {
        Toast.makeText(getActivity(), name, Toast.LENGTH_SHORT).show();
    }
}
