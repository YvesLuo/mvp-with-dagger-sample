package com.desperado.ref_base_dagger_test.di;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.desperado.ref_base_dagger_test.di.component.ApplicationComponent;
import com.desperado.ref_base_dagger_test.di.module.ActivityModule;
import com.desperado.ref_base_dagger_test.di.module.ApplicationModule;
import com.desperado.ref_base_dagger_test.di.module.FragmentModule;


/**
 * Created by desperado on 17-4-27.
 * Component通信委托类
 * 配合{@link DaggerContract}抽象出对项目基础依赖的配置
 */

public class DaggerContractDelegate {

    /**
     * 获取Application中的Component
     *
     * @param application application
     * @return ApplicationComponent
     */
    public static ApplicationComponent getAppComponent(Application application) {
        if (application instanceof DaggerContract.AppComponent) {
            return ((DaggerContract.AppComponent) application).getApplicationComponent();
        }
        throw new IllegalStateException("application doesn`t implements DaggerContract.AppComponent");
    }
    @SuppressWarnings("unchecked")
    public static <C> C getCustomAppComonent(Application application, Class<C> classType) {
        if (application instanceof DaggerContract.CustomAppComponent) {
            return classType.cast(((DaggerContract.CustomAppComponent<C>) application).getCustomAppComponent());
        }
        throw new IllegalStateException("activity doesn`t implements HasComponent.");
    }

    /**
     * 获取Fragment宿主Activity的Component
     *
     * @param activity      宿主Ac
     * @param componentType component的类型
     * @param <C>           component的类型
     * @return component
     */
    @SuppressWarnings("unchecked")
    public static <C> C getAttachActivityComponent(Activity activity, Class<C> componentType) {
        if (activity instanceof DaggerContract.HasComponent) {
            return componentType.cast(((DaggerContract.HasComponent<C>) activity).getComponent());
        }
        throw new IllegalStateException("activity doesn`t implements HasComponent.");
    }

    /**
     * 获取Application依赖的module
     *
     * @param application app
     * @return
     */
    public static ApplicationModule getAppModule(Application application) {
        return new ApplicationModule(application);
    }

    /**
     * 获取Activity依赖的module
     *
     * @param activity ac
     * @return
     */
    public static ActivityModule getAcModule(Activity activity) {
        return new ActivityModule(activity);
    }

    /**
     * 获取fragment依赖的fragmentModule
     *
     * @param context  context
     * @param fragment fragment
     * @return FragmentHasPModule
     */
    public static FragmentModule getFragmentModule(Context context, Fragment fragment) {
        return new FragmentModule(context, fragment);
    }
}
