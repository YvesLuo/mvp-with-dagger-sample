package com.desperado.ref_base_dagger_test.di.component;

import android.app.Application;
import android.content.Context;

import com.desperado.ref_base_dagger_test.di.module.ApplicationModule;
import com.desperado.ref_base_dagger_test.di.qualifier.ApplicationContext;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by desperado on 17-4-27.
 * 1.所有的Component都依赖该Component
 * 2.注意: 如果后期需要添加全局单例时, 建议另开Module, 然后添加进ApplicationComponent
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    @ApplicationContext
    Context providesApplicationContext(); //向依赖ApplicationComponent的Component暴露Context

    Application providesApplication();

}
