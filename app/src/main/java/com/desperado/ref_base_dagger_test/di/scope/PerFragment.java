package com.desperado.ref_base_dagger_test.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by desperado on 17-4-28.
 * Fragment作作用域注解
 * 所有Fragment级别的Module内的方法或者通过构造方法注入的类 都得标注上该注解
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerFragment {
}
