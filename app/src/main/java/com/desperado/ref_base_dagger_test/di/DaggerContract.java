package com.desperado.ref_base_dagger_test.di;


import com.desperado.ref_base_dagger_test.di.component.ApplicationComponent;
import com.desperado.ref_base_dagger_test.di.module.ActivityModule;

/**
 * Created by desperado on 17-4-27.
 * Component通信的接口管理类
 * 该类通过配合{@link DaggerContractDelegate}来搭建整个项目的依赖注入的基础实现
 * 具体使用见gitlab的wiki文档上
 */
public interface DaggerContract {

    /**
     * 获取ApplicationComponent接口
     */
    interface AppComponent {
        ApplicationComponent getApplicationComponent();
    }

    interface CustomAppComponent<C> {
        C getCustomAppComponent();
    }

    /**
     * 当Fragment中需要获得Activity中的Component实现注入时, 在Fragment宿主的Activity实现该接口
     *
     * @param <C> FragmentHasPModule 宿主Activity的Component类型
     */
    interface HasComponent<C> {
        C getComponent();
    }

    /**
     * 获取Fragment宿主Activity的Component
     */
    interface AttachActivityComponent {
        <C> C getAttachActivityComponent(Class<C> classType);
    }

    /**
     * 获取ActivityModule接口
     */
    interface AcModule {
        ActivityModule getAcModule();
    }

    /**
     * 获取Fragment接口
     */
    interface FragmentModule {
        com.desperado.ref_base_dagger_test.di.module.FragmentModule getFragmentModule();
    }
}
