package com.desperado.ref_base_dagger_test.di.module;

import android.app.Application;
import android.content.Context;

import com.desperado.ref_base_dagger_test.di.qualifier.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by desperado on 17-4-27.
 * 提供Application实例和ApplicationContext
 * 注意: 在写providesXX方法时, 记得要添加{@link Singleton}注解, 否则Dagger生成的实例并不是单例
 */
@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Singleton
    @Provides
    @ApplicationContext
    Context providesApplicationContext() {
        return mApplication.getApplicationContext();
    }

    @Singleton
    @Provides
    Application providesApplication() {
        return mApplication;
    }


}
