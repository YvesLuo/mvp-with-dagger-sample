package com.desperado.ref_base_dagger_test.lifecircle;

/**
 * Created by desperado on 17-4-26.
 * presenter的生命周期
 */

public final class PresenterEvent {

    public static final int ON_ATTACH = 0;

    public static final int ON_DETACH = 1;

}
