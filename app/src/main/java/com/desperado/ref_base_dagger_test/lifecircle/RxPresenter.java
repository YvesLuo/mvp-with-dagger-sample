package com.desperado.ref_base_dagger_test.lifecircle;

import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.RxLifecycle;

import javax.annotation.Nonnull;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by desperado on 17-4-26.
 * RxLifeCircle的Presenter组件
 */

public abstract class RxPresenter implements LifecycleProvider<Integer> {

    private BehaviorSubject<Integer> lifeSubject = BehaviorSubject.create();

    @Nonnull
    @Override
    public Observable<Integer> lifecycle() {
        return lifeSubject.hide();
    }

    @Nonnull
    @Override
    public <T> LifecycleTransformer<T> bindUntilEvent(@Nonnull Integer event) {
        return RxLifecycle.bindUntilEvent(lifeSubject, event);
    }

    @Nonnull
    @Override
    public <T> LifecycleTransformer<T> bindToLifecycle() {
        return RxPresenterLifeCircle.bindPresenter(lifeSubject);
    }

    protected void onCreate() {
        lifeSubject.onNext(PresenterEvent.ON_ATTACH);
    }

    protected void onDestroy() {
        lifeSubject.onNext(PresenterEvent.ON_DETACH);
    }
}
