package com.desperado.ref_base_dagger_test.lifecircle;

import android.support.annotation.NonNull;

import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.OutsideLifecycleException;
import com.trello.rxlifecycle2.RxLifecycle;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by desperado on 17-4-26.
 * 简单包装RxLifeCircle的bind方法
 */
public class RxPresenterLifeCircle {

    @NonNull
    public static <T> LifecycleTransformer<T> bindPresenter(Observable<Integer> lifeCircle) {
        return RxLifecycle.bind(lifeCircle, PRESENTER_LIFE_CIRCLE);
    }

    private static final Function<Integer, Integer> PRESENTER_LIFE_CIRCLE = new Function<Integer, Integer>() {
        @Override
        public Integer apply(@NonNull Integer integer) throws Exception {
            switch (integer) {
                case PresenterEvent.ON_ATTACH:
                    return PresenterEvent.ON_DETACH;
                case PresenterEvent.ON_DETACH:
                    throw new OutsideLifecycleException("cannot bind presenter that outside of its life circle");
                default:
                    throw new UnsupportedOperationException("unsupported bind event");
            }
        }
    };
}
