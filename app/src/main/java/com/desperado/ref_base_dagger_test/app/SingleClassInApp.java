package com.desperado.ref_base_dagger_test.app;

import javax.inject.Singleton;

/**
 * Created by root on 17-5-2.
 * 使用构造方法注入单例
 * 注意要在类上标注{@link Singleton} 才能生成单例
 * 如果
 */
public class SingleClassInApp {

    private String mName = "J罗";

    public String getName() {
        return mName;
    }
}
