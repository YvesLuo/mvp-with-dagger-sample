package com.desperado.ref_base_dagger_test.app;

import android.app.Application;
import android.widget.Toast;

import com.desperado.ref_base_dagger_test.app.di.ApplicationModule;
import com.desperado.ref_base_dagger_test.app.di.CustomApplicationComponent;
import com.desperado.ref_base_dagger_test.app.di.DaggerCustomApplicationComponent;
import com.desperado.ref_base_dagger_test.di.DaggerContract;
import com.desperado.ref_base_dagger_test.di.DaggerContractDelegate;

import javax.inject.Inject;

/**
 * Created by root on 17-5-2.
 */

public class TestApplication extends Application implements DaggerContract.CustomAppComponent<CustomApplicationComponent> {

    private CustomApplicationComponent mApplicationComponent;

    @Inject
    String mName;

    @Override
    public void onCreate() {
        super.onCreate();
        setupApplicationComponent();
        Toast.makeText(this, mName, Toast.LENGTH_SHORT).show();
    }

    private void setupApplicationComponent() {
        mApplicationComponent = DaggerCustomApplicationComponent.builder()
                .applicationModule(new ApplicationModule())
                .applicationModule(DaggerContractDelegate.getAppModule(this))
                .build();
        mApplicationComponent.inject(this);
    }

    @Override
    public CustomApplicationComponent getCustomAppComponent() {
        return mApplicationComponent;
    }
}
